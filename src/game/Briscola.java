package game;

import java.util.List;
import java.util.Optional;

enum Player { Player1, Player2 }

public interface Briscola {

    List<Optional<Card>> getDeck();

    List<Optional<Card>> getTable();

    Optional<Card> getBriscola();

    List<Optional<Card>> getPlayerHand(Player player);

    List<Optional<Card>> getPlayerPile(Player player);

    int getScore(Player player);

    void initGame();

    void playCard(Player player, Card card);

    Boolean checkHandTermination();

    Optional<Player> checkHandWinner(Player initialPlayer);

    void collectWonCards(Player winningPlayer);

    void drawNextHandCards(Player winningPlayer);

    Boolean checkGameTermination();

    Optional<Player> determineWinner();
}
