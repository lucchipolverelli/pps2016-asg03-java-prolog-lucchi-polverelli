package game

trait Card{
  def number: String

  def seed: String
}

class CardImpl(override val number: String, override val seed: String) extends Card{}

object Card {
  def apply(number: String, seed: String): Card = new CardImpl(number, seed)
}

