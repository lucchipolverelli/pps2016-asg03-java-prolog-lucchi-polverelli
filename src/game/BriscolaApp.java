package game;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.util.Optional;
import java.util.List;

public class BriscolaApp {

    private final String BACK_CARD_IMAGE = "../res/back_card.jpg";

    private final Briscola briscola;
    private final CardButton[] player1HandButtons = new CardButton[3];
    private final CardButton[] player2HandButtons = new CardButton[3];
    private final JLabel deckLabel = new JLabel();
    private final CardButton briscolaView = new CardButton();
    private final JLabel p1PileLabel = new JLabel();
    private final JLabel p2PileLabel = new JLabel();
    private final CardButton[] tableViews = new CardButton[2];
    private final JButton collectButton = new JButton("Collect");
    private final JButton exitButton = new JButton("Exit");
    private final JFrame frame = new JFrame("Briscola");
    private Player turn = Player.Player1;
    private Player initialPlayer = Player.Player1;
    private boolean finished = false;

    public BriscolaApp(Briscola briscola) throws Exception {
        this.briscola = briscola;
        this.initPane();
        this.initGame();
    }

    private void playerTurn(int i){

        this.disablePlayersCards();

        Card card;
        if(this.turn == Player.Player1){
            card = new CardImpl(this.player1HandButtons[i].getNumberText(),this.player1HandButtons[i].getSeedText());
        }else{
            card = new CardImpl(this.player2HandButtons[i].getNumberText(),this.player2HandButtons[i].getSeedText());
        }

        this.briscola.playCard(this.turn,card);
        this.updateTable();
        this.updatePlayerHand(this.turn);

        if(this.briscola.checkHandTermination()){
            this.collectButton.setEnabled(true);
        }else {
            this.changeTurn();
            this.enableCurrentPlayerCards();
        }
    }

    private void collectTableCards(){

        Optional<Player> winningHandPlayer =  this.briscola.checkHandWinner(this.initialPlayer);

        if(winningHandPlayer.isPresent()) {
            this.initialPlayer = winningHandPlayer.get();
            this.print(winningHandPlayer.get()+" collects!");
            this.briscola.collectWonCards(winningHandPlayer.get());
            this.updatePlayerPile(winningHandPlayer.get());
            this.updateTable();

            if(this.briscola.checkGameTermination()){
                Optional<Player> winningPlayer = this.briscola.determineWinner();

                this.print("Player1 score: "+this.briscola.getScore(Player.Player1));
                this.print("Player2 score: "+this.briscola.getScore(Player.Player2));

                if(winningPlayer.isPresent()) {
                    this.exitButton.setText(winningPlayer.get() + " won!");
                }else{
                    this.exitButton.setText("Even!");
                }
                this.finished=true;

            }else{

                if(!this.briscola.getDeck().isEmpty()){
                    this.briscola.drawNextHandCards(winningHandPlayer.get());
                    this.updatePlayerHand(Player.Player1);
                    this.updatePlayerHand(Player.Player2);

                    if(this.briscola.getDeck().isEmpty()){
                        this.updateDeck(deckLabel);
                        this.updateBriscola();
                    }
                }
            }
            if(this.initialPlayer != this.turn){
                this.changeTurn();
            }
        }
        this.collectButton.setEnabled(false);
        this.enableCurrentPlayerCards();
    }

    private void changeTurn(){
        this.turn = this.turn == Player.Player1 ? Player.Player2 : Player.Player1;
    }

    private void enableCurrentPlayerCards(){
        if(this.turn == Player.Player1){
            for(int j = 0; j < 3; j++){
                if(!this.player1HandButtons[j].getNumberText().equals("")) {
                    this.player1HandButtons[j].setEnabled(true);
                }
            }
        }else{
            for(int j = 0; j < 3; j++){
                if(!this.player2HandButtons[j].getNumberText().equals("")) {
                    this.player2HandButtons[j].setEnabled(true);
                }
            }
        }
    }

    private void disablePlayersCards(){
        for(int j = 0; j < 3; j++){
            this.player1HandButtons[j].setEnabled(false);
            this.player2HandButtons[j].setEnabled(false);
        }
    }

    private void print(String string){
        System.out.println(string);
    }

    private void initPane(){
        this.frame.setLayout(new BorderLayout());
        JPanel player1HandPanel = new JPanel(new GridLayout(1,3));
        JPanel player2HandPanel = new JPanel(new GridLayout(1,3));
        for (int i=0;i<3;i++){
            final int i2 = i;

            this.player1HandButtons[i]=new CardButton();
            player1HandPanel.add(this.player1HandButtons[i], i);
            this.player1HandButtons[i].addActionListener(e -> { if (!this.finished) playerTurn(i2); });

            this.player2HandButtons[i]=new CardButton();
            player2HandPanel.add(this.player2HandButtons[i], i);
            this.player2HandButtons[i].addActionListener(e -> { if (!this.finished) playerTurn(i2); });
        }
        JPanel tablePanel = new JPanel(new GridLayout(1,2));
        for (int i=0;i<2;i++) {
            this.tableViews[i] = new CardButton();
            this.tableViews[i].setEnabled(false);
            tablePanel.add(this.tableViews[i], i);
        }

        JPanel g1 = new JPanel(new GridLayout(2,1));
        g1.setPreferredSize(new Dimension(90 , 400));
        this.briscolaView.setEnabled(false);
        g1.add(BorderLayout.NORTH, this.briscolaView);
        g1.add(BorderLayout.SOUTH, this.deckLabel);

        JPanel g2 = new JPanel(new GridLayout(3,3));
        g2.add(BorderLayout.NORTH, player1HandPanel);
        g2.add(BorderLayout.CENTER,tablePanel);
        g2.add(BorderLayout.SOUTH, player2HandPanel);

        JPanel g3 = new JPanel(new GridLayout(2,1));
        g3.setPreferredSize(new Dimension(90 , 400));
        g3.add(BorderLayout.NORTH, this.p1PileLabel);
        g3.add(BorderLayout.SOUTH, this.p2PileLabel);

        JPanel s = new JPanel(new FlowLayout());
        s.add(BorderLayout.WEST,this.collectButton);
        s.add(BorderLayout.EAST,this.exitButton);
        this.collectButton.setEnabled(false);
        this.exitButton.addActionListener(e -> System.exit(0));
        this.collectButton.addActionListener(e -> this.collectTableCards());

        this.frame.add(BorderLayout.SOUTH,s);
        this.frame.add(BorderLayout.WEST,g1);
        this.frame.add(BorderLayout.CENTER,g2);
        this.frame.add(BorderLayout.EAST,g3);

        this.frame.setSize(500,400);
        this.frame.setVisible(true);
    }

    private void initGame(){
        this.briscola.initGame();
        this.updateDeck(deckLabel);
        this.updateBriscola();
        this.updatePlayerHand(Player.Player1);
        this.updatePlayerHand(Player.Player2);
        this.updateTable();
        for(CardButton b: this.player2HandButtons) {
            b.setEnabled(false);
        }
        this.print("Player1 starts");
    }

    private void updateDeck(JLabel view){
        List<Optional<Card>> deck = this.briscola.getDeck();
        this.updateBackCardImage(deck, view);
    }

    private void updatePlayerPile(Player player){
        List<Optional<Card>> pile = this.briscola.getPlayerPile(player);
        JLabel view = player == Player.Player1 ? this.p1PileLabel : this.p2PileLabel;
        this.updateBackCardImage(pile, view);
    }

    private void updateBackCardImage(List<Optional<Card>> list, JLabel view){
        if(list.isEmpty()){
            view.setIcon(null);
        } else {
            try {
                Image img = ImageIO.read(getClass().getResource(BACK_CARD_IMAGE));
                img = img.getScaledInstance( 90, 160,  java.awt.Image.SCALE_SMOOTH ) ;
                view.setIcon(new ImageIcon(img));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void updateTable() {
        List<Optional<Card>> table = this.briscola.getTable();
        for(int i = 0; i < table.size(); i++) {
            if (table.get(i).isPresent()) {
                this.tableViews[i].setNumberText(table.get(i).get().number());
                this.tableViews[i].setSeedText(table.get(i).get().seed());
            } else {
                this.tableViews[i].setNumberText("");
                this.tableViews[i].setSeedText("");
            }
        }
    }

    private void updateBriscola() {
        if (!this.briscola.getDeck().isEmpty()) {
            Optional<Card> card = this.briscola.getBriscola();
            if(card.isPresent()) {
                this.briscolaView.setNumberText(card.get().number());
                this.briscolaView.setSeedText(card.get().seed());
            }
        } else {
            this.briscolaView.setNumberText("");
            this.briscolaView.setSeedText("");
        }
    }

    private void updatePlayerHand(Player player){
        List<Optional<Card>> playerHand = this.briscola.getPlayerHand(player);
        if (player == Player.Player1) {
            for (int i = 0; i < playerHand.size(); i++) {
                Optional<Card> card = playerHand.get(i);
                if (card.isPresent()) {
                    this.player1HandButtons[i].setNumberText(card.get().number());
                    this.player1HandButtons[i].setSeedText(card.get().seed());
                }
            }
            for (int i = playerHand.size(); i < this.player1HandButtons.length; i++) {
                this.player1HandButtons[i].setNumberText("");
                this.player1HandButtons[i].setSeedText("");
            }
        } else {
            for (int i = 0; i < playerHand.size(); i++) {
                Optional<Card> card = playerHand.get(i);
                if (card.isPresent()) {
                    this.player2HandButtons[i].setNumberText(card.get().number());
                    this.player2HandButtons[i].setSeedText(card.get().seed());
                }
            }
            for (int i = playerHand.size(); i < this.player2HandButtons.length; i++) {
                this.player2HandButtons[i].setNumberText("");
                this.player2HandButtons[i].setSeedText("");
            }
        }
    }

    private class CardButton extends JButton {
        private JLabel numberLabel = new JLabel();
        private JLabel seedLabel = new JLabel();

        public CardButton(){
            super();
            this.setLayout(new BorderLayout());
            this.add(BorderLayout.NORTH,numberLabel);
            this.add(BorderLayout.CENTER,seedLabel);
        }

        public void setNumberText(String number) {
            this.numberLabel.setText(number);
        }

        public void setSeedText(String seed) {
            this.seedLabel.setText(seed);
        }

        public String getNumberText() {
            return this.numberLabel.getText();
        }

        public String getSeedText() {
            return this.seedLabel.getText();
        }
    }

    public static void main(String[] args){
        try {
            new BriscolaApp(new BriscolaImpl("src/game/briscola.pl"));
        } catch (Exception e) {
            System.out.println("Problems loading the theory");
        }
    }
}

