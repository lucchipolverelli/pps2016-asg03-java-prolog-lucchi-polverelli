package game

import Scala2P._

import alice.tuprolog.{Struct, Theory}

import java.io.FileInputStream
import java.util.Optional

import scala.collection.mutable.Buffer
import collection.JavaConverters._

class BriscolaImpl(fileName: String) extends Briscola {

  implicit private def playerToString(player: Player): String = player match {
    case Player.Player1 => "p1"
    case _ => "p2"
  }

  implicit private def stringToPlayer(s: String): Player = s match {
    case "p1" => Player.Player1
    case _ => Player.Player2
  }

  implicit private def cardToString(card: Card): String = "("+card.number+","+card.seed+")"

  implicit private def stringToCard(s: String): Card = {
    val subString = s.substring(4, s.length()-1)
    val splitString = subString.split(",")
    Card(splitString(0),splitString(1))
  }

  private val engine = mkPrologEngine(new Theory(new FileInputStream(fileName)))

  override def getDeck = {
    val term = solveOneAndGetTerm(engine, "deck(D)", "D").asInstanceOf[Struct]
    val iterator = term.listIterator()
    iterator.asScala.toList.map(_.toString).map(s => Optional.of[Card](s)).to[Buffer].asJava
  }

  override def getTable = {
    val term = solveOneAndGetTerm(engine, "table(T)", "T").asInstanceOf[Struct]
    val iterator = term.listIterator()
    iterator.asScala.toList.map(_.toString).map {
      case "null" => Optional.empty[Card]()
      case s => Optional.of[Card](s)
    }.to[Buffer].asJava
  }

  override def getBriscola = {
    val term = solveOneAndGetTerm(engine, "briscola(B)", "B").asInstanceOf[Struct]
    term.toString match{
      case "null" => Optional.empty[Card]()
      case s => Optional.of[Card](s)
    }
  }

  override def getPlayerHand(player: Player) = {
    val term = solveOneAndGetTerm(engine, s"${playerToString(player)}_hand(PH)", "PH").asInstanceOf[Struct]
    val iterator = term.listIterator()
    iterator.asScala.toList.map(_.toString).map(s => Optional.of[Card](s)).to[Buffer].asJava
  }

  override def getPlayerPile(player: Player) = {
    val term = solveOneAndGetTerm(engine, s"${playerToString(player)}_pile(PP)", "PP").asInstanceOf[Struct]
    val iterator = term.listIterator()
    iterator.asScala.toList.map(_.toString).map(s => Optional.of[Card](s)).to[Buffer].asJava
  }

  override def initGame() = {
    this.createDeck
    this.createTable
    this.drawBriscola
    this.createPlayerHand(Player.Player1)
    this.createPlayerHand(Player.Player2)
    this.createPlayerPile(Player.Player1)
    this.createPlayerPile(Player.Player2)
  }

  override def playCard(player: Player, card: Card) = {
    val goal = s"table(T),${playerToString(player)}_hand(PH),play_card(T,${playerToString(player)},${cardToString(card)},PH,NPH,NT)" +
      s",retractall(${playerToString(player)}_hand(_)),assert(${playerToString(player)}_hand(NPH)),retractall(table(_)),assert(table(NT))"
    solveWithSuccess(engine,goal)
  }

  override def checkHandTermination() = {
    val table = getTable()
    table.get(0).isPresent && table.get(1).isPresent
  }

  override def checkHandWinner(initialPlayer: Player) = {
    val term = solveOneAndGetTerm(engine, s"briscola(B),table(T),check_current_hand_winner(${playerToString(initialPlayer)},B,T,W)", "W")
      .asInstanceOf[Struct]
    Optional.of[Player](term.toString)
  }

  override def collectWonCards(winningPlayer: Player) = {
    val goal = s"${playerToString(winningPlayer)}_pile(PP),table(T),update_player_pile(PP,T,NPP)" +
      s",retractall(${playerToString(winningPlayer)}_pile(_)),assert(${playerToString(winningPlayer)}_pile(NPP))"
    solveWithSuccess(engine,goal)
    createTable()
  }

  override def drawNextHandCards(winningPlayer: Player) = {
    def _drawNewCard(player: Player) = {
      val goal = s"${playerToString(player)}_hand(PH),deck(D),briscola(B)," +
        s"add_card_to_player_hand(PH,D,B,NPH,ND),retractall(${playerToString(player)}_hand(PH)),assert(${playerToString(player)}_hand(NPH))," +
        s"retractall(deck(D)),assert(deck(ND))"
      solveWithSuccess(engine,goal)
    }

    winningPlayer match {
      case Player.Player1 =>
        _drawNewCard(Player.Player1)
        _drawNewCard(Player.Player2)
      case _ =>
        _drawNewCard(Player.Player2)
        _drawNewCard(Player.Player1)
    }

  }

  override def checkGameTermination() = {
    val term1 = solveOneAndGetTerm(engine, s"${playerToString(Player.Player1)}_hand(PH)", "PH").asInstanceOf[Struct]
    val term2 = solveOneAndGetTerm(engine, s"${playerToString(Player.Player2)}_hand(PH)", "PH").asInstanceOf[Struct]
    val iterator1 = term1.listIterator()
    val iterator2 = term2.listIterator()
    iterator1.asScala.toList.isEmpty && iterator2.asScala.toList.isEmpty
  }

  override def determineWinner() = {
    val scorePlayer1 = getScore(Player.Player1)
    val scorePlayer2 = getScore(Player.Player2)

    if (scorePlayer1 > scorePlayer2) Optional.of[Player](Player.Player1)
    else if (scorePlayer1 < scorePlayer2) Optional.of[Player](Player.Player2)
    else Optional.empty[Player]()
  }

  override def getScore(player: Player) = {
    val term = solveOneAndGetTerm(engine,s"${playerToString(player)}_pile(PP),count_score(PP,S)", "S")
    term.toString.toInt
  }

  private def createDeck() = {
    val goal = "retractall(deck(_)),create_deck(D),assert(deck(D))"
    solveWithSuccess(engine,goal)
  }

  private def createTable() = {
    val goal = "retractall(table(_)),create_table(T),assert(table(T))"
    solveWithSuccess(engine,goal)
  }

  private def drawBriscola() = {
    val goal = "deck(D),draw_briscola(D,B,ND),assert(briscola(B)),retractall(deck(_)),assert(deck(ND))"
    solveWithSuccess(engine,goal)
  }

  private def createPlayerHand(player: Player) = {
    val goal = s"deck(D),create_player_hand(D,PH,ND),assert(${playerToString(player)}_hand(PH)),retractall(deck(D))," +
      "assert(deck(ND))"
    solveWithSuccess(engine,goal)
  }

  private def createPlayerPile(player: Player) = {
    val goal = s"assert(${playerToString(player)}_pile([]))"
    solveWithSuccess(engine,goal)
  }

}
