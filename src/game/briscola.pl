% card_seed(?Seed): seed of a card
card_seed(bastoni).
card_seed(spade).
card_seed(coppe).
card_seed(denari).

% card_number(?Number): number of a card
card_number(asso).
card_number(2).
card_number(3).
card_number(4).
card_number(5).
card_number(6).
card_number(7).
card_number(fante).
card_number(cavallo).
card_number(re).

% card_value(?CardNumber,?CardValue): value of a card
card_value(asso,10).
card_value(2,1).
card_value(3,9).
card_value(4,2).
card_value(5,3).
card_value(6,4).
card_value(7,5).
card_value(fante,6).
card_value(cavallo,7).
card_value(re,8).

% card_point(?CardNumber,?CardPoint): point of a card
card_point(asso,11) :- !.
card_point(3,10) :- !.
card_point(re,4) :- !.
card_point(cavallo,3) :- !.
card_point(fante,2) :- !.
card_point(N,0) :- card_number(N).

% card(+Number,+Seed): card of the game
card(N,S):- card_number(N), card_seed(S).

% result(+OnBoard,-OnScreen): result of a game
result(even,"even!").
result(p1,"Player 1 wins").
result(p2,"Player 2 wins").

% other_player(?Player,?OtherPlayer)
other_player(p1,p2).
other_player(p2,p1).

% create_same_elements_list(+ElementNumber,+ListElements,-List): creates a list with same elements
create_same_elements_list(0,_,[]) :- !.
create_same_elements_list(N,X,[X|T]) :- N2 is N-1, create_same_elements_list(N2,X,T).

% create_table(-Table): creates an initially empty table
create_table(T) :- create_same_elements_list(2,null,T).

% update_table(+Table,+Player,+Card,-NewTable): updates the table adding a card
update_table([null,X],p1,C,[C,X]).
update_table([X,null],p2,C,[X,C]).

% update_player_pile(+PlayerPile,+CardList,-NewPlayerPile): updates the player pile adding collected cards
update_player_pile(PP,CL,NPP) :- append(PP,CL,NPP).

% create_deck(-Deck): creates an initially full deck
create_deck(D) :- create_list(40,D,_).
create_list(0,[],_).
create_list(N,[(X1,X2)|T],L) :- N2 is N-1, card(X1,X2), not_member((X1,X2),L), create_list(N2,T,[(X1,X2)|L]), !.
not_member(_,[]) :- !.
not_member(X,[H|T]) :- X \= H, not_member(X,T).

% update_deck(+Deck,+Card,-NewDeck): updates the deck when a card is drawn
update_deck(D,C,ND) :- delete(C,D,ND).

% draw_card(+Deck,-Card): draws a random card from the deck
draw_card(D,C) :- length(D,N), rand_int(N,I), I2 is I + 1, element(I2,D,C).

% create_player_hand(+Deck,-PlayerHand,-NewDeck): creates the initial player hand with 3 cards
create_player_hand(D,NPH,ND) :- add_cards(3,[],D,NPH,ND).
add_cards(0,PH,D,PH,D) :- !.
add_cards(N,PH,D,NPH,ND) :- N2 is N - 1, add_card_to_player_hand(PH,D,_,PH1,D1), add_cards(N2,PH1,D1,NPH,ND).

% remove_card_from_player_hand(+PlayerHand,+Card,-NewPlayerHand): removes the card form the player hand when it is played
remove_card_from_player_hand(PH,C,NPH) :- delete(C,PH,NPH).

% add_card_to_player_hand(+PlayerHand,+Deck,+Briscola,-NewPlayerHand,-NewDeck): adds a card to the player hand, if the deck is empty the
% last player will draw the briscola
add_card_to_player_hand(PH,[],B,NPH,[]) :- append(PH,[B],NPH), !.
add_card_to_player_hand(PH,D,_,NPH,ND) :- draw_card(D,C), append(PH,[C],NPH), update_deck(D,C,ND).

% check_current_hand_winner(+InitialPlayer,+Briscola,+Table,-WinningPlayer): checks the winner of the current hand
check_current_hand_winner(_,_,[(N1,S1),(N2,S2)],W) :- S1 = S2, card_value(N1,V1), card_value(N2,V2), V1 < V2, W = p2, !.
check_current_hand_winner(_,_,[(N1,S1),(N2,S2)],W) :- S1 = S2, card_value(N1,V1), card_value(N2,V2), V1 > V2, W = p1, !.
check_current_hand_winner(_,(_,B),[(_,S1),_],W) :- S1 = B, W = p1, !.
check_current_hand_winner(_,(_,B),[_,(_,S2)],W) :- S2 = B, W = p2, !.
check_current_hand_winner(P,_,_,W) :- W = P.

% draw_briscola(+Deck,-Briscola,-NewDeck): draws a card from the deck which will be the briscola
draw_briscola(D,B,ND) :- draw_card(D,B), update_deck(D,B,ND).

% play_card(+Table,+Player,+Card,+PlayerHand,-NewPlayerHand,-NewTable): a player plays a card
play_card(T,P,C,PH,NPH,NT) :- remove_card_from_player_hand(PH,C,NPH), update_table(T,P,C,NT).

% count_score(+PlayerPile,-Score): counts the score of a player
count_score([],0).
count_score([(N,_)|T],S) :- card_point(N,P), count_score(T,S1), S is S1 + P.